'use strict';

const dliveInit = require('./dliveInit');
const config = require('./config');
const logger = require('./logger');

class dliver extends dliveInit {
	constructor(channel, authKey) {
		super(channel, authKey);
		this.initalized = true;
	}

	sendMessage(message) {
		message = `${config.message_prefix}${message}`;
		logger.debug('Sending message: ' + message);
		if (!this.initalized) return new Error('You have to create a class instance first!');
		return this.sendChatMessage(message);
	}

	sendMessageToChannel(channel, message) {
		message = `${config.message_prefix}${message}`;
		logger.debug('Sending message: ' + message + ' to the channel: ' + channel);
		if (!this.initalized) return new Error('You have to create a class instance first!');
		return this.sendMessageToChannelChat(channel, message);
	}

	getChannelInformation(displayname, callback) {
		if (!this.initalized) return new Error('You have to create a class instance first!');
		return this.getChannelInformationByDisplayName(displayname, (result) => {
			logger.debug(`Channel information for ${displayname}: ${result}`);
			callback(result);
		});
	}

	getChannelTopContributors(displayname, amountToShow, rule, callback) {
		if (!this.initalized) return new Error('You have to create a class instance first!');
		return this.getChannelTopContributorsByDisplayName(displayname, amountToShow, rule, (result) => {
			logger.debug(`Channel top contributors for ${displayname}: ${result}`);
			callback(result);
		});
	}

}

module.exports = dliver;
