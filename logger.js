'use strict';

const { createLogger, format, transports } = require('winston');
const path = require('path');

const logger = createLogger({
	level: process.env.NODE_ENV === 'production' ? 'info' : 'debug',
	format: format.combine(
		format.label({ label: path.basename(process.mainModule.filename) }),
		format.colorize(),
		format.timestamp({
			format: 'YYYY-MM-DD HH:mm:ss'
		}),
		format.printf(info => `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`)
	),
	transports: [
		new transports.File({ filename: 'error.log', level: 'error' }),
		new transports.File({ filename: 'debug.log' })
	]
});

if (process.env.NODE_ENV !== 'production') {
	logger.add(new transports.Console({
		format: format.combine(
			format.label({ label: path.basename(process.mainModule.filename) }),
			format.colorize(),
			format.timestamp({
				format: 'YYYY-MM-DD HH:mm:ss'
			}),
			format.printf(info => `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`)
		),
	}));
}

module.exports = logger;
