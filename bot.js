'use strict';

const dlive = require('./index.js');
const config = require('./config');
const format = require('string-format');
const logger = require('./logger');

logger.info('started');

format.extend(String.prototype, {});
const bots = [];
config.channels.forEach(channel => bots.push(new dlive(channel, config.authkey)));

bots.forEach(bot => {
	if (config.on.startup !== '') bot.sendMessage(config.on.startup);
	if (config.on.live !== '') bot.on('ChatLive', () => bot.sendMessage(config.on.live));
	if (config.on.end !== '') bot.on('ChatOffline', () => bot.sendMessage(config.on.end));
	if (config.on.follow !== '') bot.on('ChatFollow', msg => bot.sendMessage(config.on.follow.format(msg)));
	if (config.on.lemon !== '') bot.on('ChatGift', msg => msg.gift === 'LEMON' && bot.sendMessage(config.on.lemon.format(msg)));
	if (config.on.icecream !== '') bot.on('ChatGift', msg => msg.gift === 'ICE_CREAM' && bot.sendMessage(config.on.icecream.format(msg)));
	if (config.on.diamond !== '') bot.on('ChatGift', msg => msg.gift === 'DIAMOND' && bot.sendMessage(config.on.diamond.format(msg)));
	if (config.on.ninjaghini !== '') bot.on('ChatGift', msg => msg.gift === 'NINJAGHINI' && bot.sendMessage(config.on.ninjaghini.format(msg)));
	if (config.on.ninjet !== '') bot.on('ChatGift', msg => msg.gift === 'NINJET' && bot.sendMessage(config.on.ninjet.format(msg)));

	config.replies.forEach(reply => bot.on('ChatText', message => message.content.toLowerCase() === reply[0] && bot.sendMessage(reply[1])));

});
