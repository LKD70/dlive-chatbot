How to contribute to this project
----

### Found a bug?
Bug squashers are very important to me, so good job! Now let's get down to reporting the bug

First of all, please search the [issues section](https://gitlab.com/LKD70/dlive-chatbot/issues) to ensure the bug hasn't been reported beforehand. If the issue does already exist, great! be sure to give it a like and if needed, indicate that you too have ran in to the issue.

**Issue not found? Nice! You've caught a fresh one!**
Let's take a look at how to correctly report it...
1. [Open an issue](https://gitlab.com/LKD70/dlive-chatbot/issues/new)
2. Ensure to give it a good, to the point and informative title.
3. Describe your issue with as much detail as possible. Please add code snippets demonstrating the issue if it's needed.

### Fixed a bug?
First of all, well done! That's pretty impressive. If your bug isn't already known, please first file an issue as shown above.
1. Open a pull request featuring your fix.
2. Add a detailed description that clearly describes the issue and how you solved it. Please include the issue number if possible.
3. Before submitting, please ensure your code meets our eslint and editorconfig standards.

### Made a change that you think we should adopt?
So you've added your own feature, eh? A module system with a module that boils your kettle? A cow emoji that orders you milk through amazon dash? That's pretty cool!
Please understand, just becuase you've made something amazing, doesn't mean it will be added. It can be amazing in its own right, if I feel it's not right for this project I likely won't include it.
That being said, i'm open to suggestions as long as you've completed your code and it follows the standards layed out in the eslint and editorconfig files. Feel free to make a pull request.

