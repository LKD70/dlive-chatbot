'use strict';

const configuration = {
	authkey: '',
	channels: [ 'dlive-1234567891', 'dlive-1234567890' ],
	message_prefix: '[BOT] - ',
	on: {
		startup: '',
		live: 'Going live in five, see you soon!',
		end: 'Thanks for the great stream everyone, see you next time!',
		follow: 'Welcome {sender.displayname}, Thanks for the follow!',
		lemon: ' // Thanks for the lemon {sender.displayname}!',
		icecream: 'Thanks for the icecream {sender.displayname}!',
		diamond: 'Thanks for the diamond {sender.displayname}!',
		ninjaghini: 'Thanks for the ninjaghini {sender.displayname}!',
		ninjet: 'Thanks for the ninjet {sender.displayname}!'
	},
	replies: [
		[ 'hello', 'Hey!' ],
		[ 'hi', 'Hey!' ],
		[ 'hey', 'Hey!' ],
		[ 'yo', 'Hey!' ],
		[ 'F4F', 'Sorry, we don\'t support follow-4-follow channels here, best of luck!' ],
	]
};

module.exports = configuration;
