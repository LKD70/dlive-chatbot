DLive Chat Bot
======

<aside class="warning">
Since writing this I've been informed by an official DLive gaurdian that running any sort of a "chat bot" at the moment isn't allowed other than tidylabs. This is due to there not being a public API available as of now.
As well as this, using a second account as the bot would be classed as multi-accounting to avade a ban, which too is a violation of the community guidelines.
</aside>

This is a DLive bot based loosly on the dlivetv-unofficial-api by timedotcc. All credits where they are due.
The bot has a few simple and configurable functions at the moment, including:
* reply to specific messages
* message in chat on events such as: going live/offline, starting the bot
* thank users when they follow/donate

## Getting Started
To install the bot, you'll first need to download and install [Node.js](https://nodejs.org/en)

1. clone/download this project.
2. open a terminal/CMD in the install directory and run `npm i` to install the required packages.
3. copy `config.example.js` to `config.js`.
4. Open `config.js` and add your `authkey` and channels. (Not sure how to get your auth key? Click [here](#getting-my-authkey)
5. Once saving your changes, start the bot using the command: `node bot.js`

### Getting my authkey
If you're not sure what an authkey is or how to get it, this is for you!
1. Navigate to your DLive channel.
2. Open the chrome/firefox developer console. (F12 or CTRL+SHIFT+C)
3. In chrome, click the Network tab and start recording traffic with the CHR filter active. In firefox just select the network tab.

![Image showing chrome networking tab and filter](https://camo.githubusercontent.com/2484eeb8dfb887df729a92e7cc4db73c29ddd232/68747470733a2f2f692e696d6775722e636f6d2f664d3864507a392e706e67)


4. reload/refresh the page
5. You'll see a long list of entries appear, one of the very first ones should have a name similar to `graphigo.prd.dlive.tv`, select it.

![Image showing chrome auth key entry](https://camo.githubusercontent.com/cc74023bce8ca1f3dfae232722c027aa0c63bd55/68747470733a2f2f692e696d6775722e636f6d2f41644b626958362e706e67)

6. In the selected option, view the "headers" and look for a header called `authorization`.
7. Copy the authorization text, this is your auth key!
